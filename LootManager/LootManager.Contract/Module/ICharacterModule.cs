﻿using LootManager.DTO.Response;
using System;
using System.Collections.Generic;
using System.Text;

namespace LootManager.Contract.Module
{
    public interface ICharacterModule
    {
        DtoGetCharacterResponse GetCharacter(int id);
    }
}
