﻿using LootManager.DTO.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace LootManager.Contract.Repository
{
    public interface ICharacterRepository
    {
        CharacterEntity GetCharacter(int id);
    }
}
