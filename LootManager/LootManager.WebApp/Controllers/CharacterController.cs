﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LootManager.Contract.Module;
using Microsoft.AspNetCore.Mvc;

namespace LootManager.WebApp.Controllers
{
    public class CharacterController : Controller
    {
        private ICharacterModule _characterModule { get; set; }
        public CharacterController(ICharacterModule characterModule)
        {
            _characterModule = characterModule;
        }

        public IActionResult CharacterDetailsView(int id)
        {
            var result  = _characterModule.GetCharacter(id);


            //map result to a ViewModel
            var resultViewModel = new Object();
            return View(resultViewModel);
        }
    }
}
