﻿using LootManager.Contract.Module;
using LootManager.Contract.Repository;
using LootManager.DTO.Model;
using LootManager.DTO.Response;
using LootManager.Infrastructure.Response;
using System;
using System.Collections.Generic;
using System.Text;

namespace LootManager.Module
{
    public class CharacterModule : ICharacterModule
    {
        private ICharacterRepository _characterRepository;


        public CharacterModule(ICharacterRepository characterRepository)
        {
            _characterRepository = characterRepository;
        }

        public DtoGetCharacterResponse GetCharacter(int id)
        {
            try
            {
                var data = _characterRepository.GetCharacter(id);

                if (data.Id == 0) return new DtoGetCharacterResponse() { Status = EnumGetCharacterResponse.NotFound };

                var charModel = new CharacterModel()
                {
                    Id = data.Id,
                    Name = data.Name,
                    //IsProgress = data.IsProgress,
                    //Owner = data.Owner
                };
                return new DtoGetCharacterResponse()
                {
                    Character = charModel,
                    Status = EnumGetCharacterResponse.Success
                };
            } catch(Exception ex)
            {
                //logError TODO
                return new DtoGetCharacterResponse() { Status = EnumGetCharacterResponse.Error };
            }

        }
    }
}
