﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LootManager.DTO.Model
{
    public class CharacterModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public PlayerModel Owner { get; set; }
        public bool IsProgress { get; set; }

        //TODO add Class/Spec
    }
}
