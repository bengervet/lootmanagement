﻿using LootManager.DTO.Model;
using LootManager.Infrastructure.Response;
using System;
using System.Collections.Generic;
using System.Text;

namespace LootManager.DTO.Response
{
    public class DtoGetCharacterResponse
    {
        public CharacterModel Character { get; set; }
        public EnumGetCharacterResponse Status { get; set; }
    }
}
