﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LootManager.DTO.Entity
{
    public class CharacterEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ClassId { get; set; }
        public int ClassSpecId { get; set; }
    }
}
