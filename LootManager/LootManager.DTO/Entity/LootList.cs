﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LootManager.DTO.Entity
{
    public class LootList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int LootPageId { get; set; }
        public int ListTypeId { get; set; } // LC, SK, ...

    }
}
