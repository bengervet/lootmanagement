﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LootManager.DTO.Entity
{
    public class LootEntity
    {
        public int Id { get; set; }
        public string WowHeadId { get; set; }
        public string Name { get; set; }
        public int SlotId { get; set; }
    }
}
