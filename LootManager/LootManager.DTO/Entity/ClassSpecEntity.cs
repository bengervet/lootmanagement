﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LootManager.DTO.Entity
{
    public class ClassSpecEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
