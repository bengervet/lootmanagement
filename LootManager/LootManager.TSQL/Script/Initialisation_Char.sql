﻿--Initialisation Script
-- To play once in a new Database to set the Default values in the tables
-- https://docs.google.com/spreadsheets/d/1mDlSvPrwGKmZd3DY8X10d7OAiNDpnPC0ctAPhrkH4To/edit?usp=sharing
-- Sheet "Insert Char"
-- TODO : init player and affect them or do that later by hand ?
-- TODO check list and update it with roster latest.



INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Swenky', 0, 8, 23)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Abrouzak', 0, 8, 23)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Druidecalme', 0, 8, 23)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Titiamzt', 1, 8, 24)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Erasa', 1, 8, 23)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Holler', 1, 8, 23)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Toulak', 1, 8, 23)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Neeshjaa', 1, 8, 23)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Lougosh', 1, 7, 21)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Balder', 1, 7, 21)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Kreggan', 1, 7, 21)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Doomro', 1, 7, 21)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Gradub', 0, 7, 21)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Phalange', 1, 6, 17)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Sargramor', 1, 6, 17)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Donzeze', 1, 6, 17)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Zalana', 1, 6, 17)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Diztinqai', 0, 6, 17)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Playmobill', 0, 6, 17)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Chauvi', 1, 6, 17)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Pewpewbat', 0, 6, 17)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Prolixe', 1, 5, 14)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Molomolo', 0, 5, 14)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Memory', 1, 5, 14)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Maddhy', 1, 5, 14)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Véronique', 0, 5, 14)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Hotei', 1, 5, 14)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Erétikobuché', 0, 5, 14)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Argetlam', 1, 4, 12)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Faya', 1, 4, 12)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Numa', 1, 4, 12)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Symbiio', 1, 4, 12)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Mickjager', 0, 4, 12)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Rukya', 1, 4, 12)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Rumeur', 1, 4, 12)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Seltarosh', 1, 3, 10)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Zoltan', 0, 3, 9)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Weezipboom', 0, 3, 10)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Daorc', 1, 3, 10)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Boutch', 0, 3, 10)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Sbob', 1, 3, 10)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Vélaxia', 1, 3, 10)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Misanimaj', 1, 3, 10)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Nenos', 1, 3, 11)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Resus', 1, 2, 6)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Corrosif', 1, 2, 6)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('AfkPetAuto', 1, 2, 8)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Gentille', 1, 1, 5)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Manicheal', 1, 1, 4)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Dukka', 1, 1, 5)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Souther', 1, 1, 2)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Groboudgra', 0, 1, 1)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Kran', 1, 1, 3)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Abrou', 1, 1, 2)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Edmon', 1, 1, 5)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Degling', 1, 1, 1)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Forcemustang', 0, 1, 3)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Milengetaou', 1, 1, 1)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Khemas', 0, 1, 1)
INSERT dbo.Character(Name, IsProgress, ClassId, ClassSpecId) VALUES ('Aldéide', 1, 1, 1)














