﻿CREATE TABLE [dbo].[LootListItem]
(
    [LootId]     INT          NOT NULL,
    [LootListId] INT          NOT NULL,
    PRIMARY KEY CLUSTERED ([LootId] ASC, [LootListId] ASC),
    CONSTRAINT [FK_LOOTLISTITEM_LOOT] FOREIGN KEY ([LootId]) REFERENCES [dbo].[Loot] ([Id]),
    CONSTRAINT [FK_LOOTLISTITEM_LOOTLIST] FOREIGN KEY ([LootListId]) REFERENCES [dbo].[LootList] ([Id])
)
