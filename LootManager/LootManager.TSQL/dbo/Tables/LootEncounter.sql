﻿CREATE TABLE [dbo].[LootEncounter] (
    [LootId]   INT        NOT NULL,
    [EncounterId]   INT        NOT NULL,
    [BossId]   INT        NULL,
    [DropRate] FLOAT (53) NULL,
    PRIMARY KEY CLUSTERED ([EncounterId] ASC, [LootId] ASC),
    CONSTRAINT [FK_LOOTENCOUNTER_BOSS] FOREIGN KEY ([BossId]) REFERENCES [dbo].[Boss] ([Id]),
    CONSTRAINT [FK_LOOTENCOUNTER_ENCOUNTER] FOREIGN KEY ([EncounterId]) REFERENCES [dbo].[Encounter] ([Id]),
    CONSTRAINT [FK_LOOTENCOUNTER_LOOT] FOREIGN KEY ([LootId]) REFERENCES [dbo].[Loot] ([Id])
);

