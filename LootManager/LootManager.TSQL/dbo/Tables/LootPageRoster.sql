﻿CREATE TABLE [dbo].[LootPageRoster] (
    [LootPageId]  INT NOT NULL,
    [ClassSpecId] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([LootPageId] ASC, [ClassSpecId] ASC),
    CONSTRAINT [FK_LOOTPAGEROSTER_CLASSSPEC] FOREIGN KEY ([ClassSpecId]) REFERENCES [dbo].[ClassSpec] ([Id]),
    CONSTRAINT [FK_LOOTPAGEROSTER_LOOTPAGE] FOREIGN KEY ([LootPageId]) REFERENCES [dbo].[LootPage] ([Id])
);

