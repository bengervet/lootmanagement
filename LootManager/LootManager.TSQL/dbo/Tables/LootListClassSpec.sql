﻿CREATE TABLE [dbo].[LootListClassSpec]
(
    [LootListId]     INT          NOT NULL,
    [CLassSpecId] INT          NOT NULL,
    PRIMARY KEY CLUSTERED ([LootListId] ASC, [CLassSpecId] ASC),
    CONSTRAINT [FK_LOOTLISTCLASSSPEC_LOOTLIST] FOREIGN KEY ([LootListId]) REFERENCES [dbo].[LootList] ([Id]),
    CONSTRAINT [FK_LOOTLISTCLASSSPEC_CLASSSPEC] FOREIGN KEY ([ClassSpecId]) REFERENCES [dbo].[ClassSpec] ([Id])
)
