﻿CREATE TABLE [dbo].[ClassSpec] (
    [Id]              INT          IDENTITY (1, 1) NOT NULL,
    [Name]            VARCHAR (50) NULL,
    [ClassSpecRoleId] INT          NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CLASSSPEC_CLASSSPECROLE] FOREIGN KEY ([ClassSpecRoleId]) REFERENCES [dbo].[ClassSpecRole] ([Id])
);



