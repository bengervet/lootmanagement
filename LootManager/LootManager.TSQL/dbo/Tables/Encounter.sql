﻿CREATE TABLE [dbo].[Encounter]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [ZoneId] INT NOT NULL, 
    [Name] VARCHAR(30) NOT NULL, 
    [Order] INT NOT NULL,
    CONSTRAINT [FK_ENCOUNTER_ZONE] FOREIGN KEY ([ZoneId]) REFERENCES [dbo].[Zone] ([Id]),
)
