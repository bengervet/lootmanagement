﻿CREATE TABLE [dbo].[LootList] (
    [Id]             INT          IDENTITY (1, 1) NOT NULL,
    [Name]           VARCHAR (50) NULL,
    [LootPageId]     INT          NULL,
    [LootListTypeId] INT          NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_LOOTLIST_LOOTLISTTYPE] FOREIGN KEY ([LootListTypeId]) REFERENCES [dbo].[LootListType] ([Id]),
    CONSTRAINT [FK_LOOTLIST_LOOTPAGE] FOREIGN KEY ([LootPageId]) REFERENCES [dbo].[LootPage] ([Id])
);

