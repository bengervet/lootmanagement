﻿CREATE TABLE [dbo].[Loot] (
    [Id]           INT          NOT NULL,
    [Name]         VARCHAR (50) NOT NULL,
    [LootSlotId]   INT          NOT NULL,
    [ClassSkillId] INT          NULL,
    [IsToken]      BIT          NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_LOOT_CLASSSKILL] FOREIGN KEY ([ClassSkillId]) REFERENCES [dbo].[ClassSkill] ([Id]),
    CONSTRAINT [FK_LOOT_LOOTSLOT] FOREIGN KEY ([LootSlotId]) REFERENCES [dbo].[LootSlot] ([Id])
);

