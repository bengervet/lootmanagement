﻿CREATE TABLE [dbo].[ClassSpecList] (
    [ClassId]     INT NOT NULL,
    [ClassSpecId] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([ClassId] ASC, [ClassSpecId] ASC),
    CONSTRAINT [FK_CLASSSPECLIST_CLASS] FOREIGN KEY ([ClassId]) REFERENCES [dbo].[Class] ([Id]),
    CONSTRAINT [FK_CLASSSPECLIST_CLASSSPEC] FOREIGN KEY ([ClassSpecId]) REFERENCES [dbo].[ClassSpec] ([Id])
);

