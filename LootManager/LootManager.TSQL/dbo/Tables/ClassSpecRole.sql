﻿CREATE TABLE [dbo].[ClassSpecRole] (
    [Id]   INT          IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (15) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

