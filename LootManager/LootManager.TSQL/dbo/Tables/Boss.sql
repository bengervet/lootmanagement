﻿CREATE TABLE [dbo].[Boss] (
    [Id]     INT          NOT NULL,
    [Name]   VARCHAR (50) NOT NULL,
    [ZoneId] INT          NOT NULL,
    [EncounterId] INT NOT NULL, 
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_BOSS_RAID] FOREIGN KEY ([ZoneId]) REFERENCES [dbo].[Zone] ([Id]),
    CONSTRAINT [FK_BOSS_ENCOUNTER] FOREIGN KEY ([EncounterId]) REFERENCES [dbo].[Encounter] ([Id]),
);

