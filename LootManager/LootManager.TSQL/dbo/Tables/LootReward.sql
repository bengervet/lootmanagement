﻿CREATE TABLE [dbo].[LootReward] (
    [LootTokenId]  INT NOT NULL,
    [LootRewardId] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([LootTokenId] ASC, [LootRewardId] ASC),
    CONSTRAINT [FK_LOOTREWARD_LOOT] FOREIGN KEY ([LootRewardId]) REFERENCES [dbo].[Loot] ([Id]),
    CONSTRAINT [FK_LOOTTOKEN_LOOT] FOREIGN KEY ([LootTokenId]) REFERENCES [dbo].[Loot] ([Id])
);

