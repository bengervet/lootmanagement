﻿CREATE TABLE [dbo].[LootSequence]
(
    [Id]     INT          IDENTITY (1, 1) NOT NULL,
    [LootId]   INT  NOT NULL,
    [LootListId] INT          NOT NULL,
    [ClassSpecId] INT          NOT NULL,
    [Order] INT NOT NULL,
    [IsAffected] BIT NOT NULL,
    [LootArchiveId] INT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_LOOTSEQUENCE_CLASSSPEC] FOREIGN KEY ([ClassSpecId]) REFERENCES [dbo].[ClassSpec] ([Id]),
    CONSTRAINT [FK_LOOTSEQUENCE_LOOT] FOREIGN KEY ([LootId]) REFERENCES [dbo].[Loot] ([Id]),
    CONSTRAINT [FK_LOOTSEQUENCE_LOOTLIST] FOREIGN KEY ([LootListId]) REFERENCES [dbo].[LootList] ([Id]),
    CONSTRAINT [FK_LOOTSEQUENCE_ARCHIVE] FOREIGN KEY ([LootArchiveId]) REFERENCES [dbo].[LootArchive] ([Id]),
)
