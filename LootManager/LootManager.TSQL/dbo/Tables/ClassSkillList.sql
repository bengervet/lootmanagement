﻿CREATE TABLE [dbo].[ClassSkillList] (
    [ClassId]      INT NOT NULL,
    [ClassSkillId] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([ClassId] ASC, [ClassSkillId] ASC),
    CONSTRAINT [FK_CLASSSKILLLIST_CLASS] FOREIGN KEY ([ClassId]) REFERENCES [dbo].[Class] ([Id]),
    CONSTRAINT [FK_CLASSSKILLLIST_CLASSSKILL] FOREIGN KEY ([ClassSkillId]) REFERENCES [dbo].[ClassSkill] ([Id])
);

