﻿CREATE TABLE [dbo].[Character] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [Name]        VARCHAR (30) NOT NULL,
    [PlayerId]      INT NOT NULL,
    [ClassId]     INT          NOT NULL,
    [ClassSpecId] INT          NOT NULL,
    [IsProgress]  BIT          NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CHARACTER_CLASS] FOREIGN KEY ([ClassId]) REFERENCES [dbo].[Class] ([Id]),
    CONSTRAINT [FK_CHARACTER_CLASSSPEC] FOREIGN KEY ([ClassSpecId]) REFERENCES [dbo].[ClassSpec] ([Id]),
    CONSTRAINT [FK_CHARACTER_PLAYER] FOREIGN KEY ([PlayerId]) REFERENCES [dbo].[Player] ([Id])
);

