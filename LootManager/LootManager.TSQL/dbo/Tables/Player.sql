﻿CREATE TABLE [dbo].[Player] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [Name]        VARCHAR (30) NOT NULL,
    [AccountId]      INT NOT NULL,
    [RoleId] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
);

