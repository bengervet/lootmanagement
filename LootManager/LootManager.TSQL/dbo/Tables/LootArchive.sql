﻿CREATE TABLE [dbo].[LootArchive] (
    [Id]          INT      IDENTITY (1, 1) NOT NULL,
    [LootId]      INT      NOT NULL,
    [CharacterId] INT      NOT NULL,
    [Date]        DATETIME NOT NULL,
    [LootListId]  INT      NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_LOOTARCHIVE_CHARACTER] FOREIGN KEY ([CharacterId]) REFERENCES [dbo].[Character] ([Id]),
    CONSTRAINT [FK_LOOTARCHIVE_LOOT] FOREIGN KEY ([LootId]) REFERENCES [dbo].[Loot] ([Id]),
    CONSTRAINT [FK_LOOTARCHIVE_LOOTLIST] FOREIGN KEY ([LootListId]) REFERENCES [dbo].[LootList] ([Id])
);

