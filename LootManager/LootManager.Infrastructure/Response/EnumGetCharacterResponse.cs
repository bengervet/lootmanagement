﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LootManager.Infrastructure.Response
{
    public enum EnumGetCharacterResponse
    {
        Success,
        NotFound,
        Error
    }
}
