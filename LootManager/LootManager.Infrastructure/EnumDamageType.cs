﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LootManager.Infrastructure
{
    [Flags]
    public enum EnumDamageType
    {
        Range = 1,
        Melee = 2,
        Magic = 4,
        Phys = 8
    }
}
