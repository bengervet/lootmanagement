﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LootManager.Infrastructure
{
    [Flags]
    public enum EnumClassSkill
    {
        OneHand = 1,
        OffHand = 2,
        TwoHands = 4,
        Cloth = 8,
        Leather = 16,
        Mail = 32,
        Plate = 64,
        Ranged = 128,
    }

    [Flags]
    public enum EnumClassWeaponSkill
    {
        OneHandedSword = 1,
        OneHandedMace = 2,
        OneHandedAxe = 4,
        TwoHandedSword = 8,
        TwoHandedMace = 16,
        TwoHandedAxe = 32,
        Fist = 64,
        Dagger = 128,
        Polearm = 256,
        Staff = 512,
        Wand = 1024,
        Gun = 2048,
        Bow = 4096,
        Crossbow = 8192,
        Thrown = 16384
    }
}
