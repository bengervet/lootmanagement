﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LootManager.Infrastructure
{
    public enum EnumRaid
    {
        NONE = 0,
        MC = 1,
        BWL = 2,
        ZG = 4,
        AQ20 = 8,
        AQ40 = 16,
        NAX = 32,
    }
}
