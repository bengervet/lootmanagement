﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LootManager.Infrastructure.Classe
{
    public enum EnumClasse
    {
        Warrior,
        Warlock,
        Priest,
        Shaman,
        Hunter,
        Mage,
        Druid,
        Rogue,
    }
}
