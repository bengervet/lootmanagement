﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LootManager.Infrastructure.Classe
{
    public enum EnumSpec
    {
        //Warrior
        Arms,
        Fury_2H,
        Fury_1H,
        Fury_DP,
        DP,

        //Warlock
        Destruction,
        Twins,

    }
}
